const sumar = require('../index');
const assert = require('assert'); 


describe("Probar la suma de 2 números", ()=>{
    // Aformar que 5+5 son 10
    it("Cinco más cinco es 10", ()=>{
        assert.equal(10, sumar(5,5));
    });

    //Afirmamos que cinco más cinco no son 55
    it("Cinco más cinco no son 55", ()=>{
        assert.notEqual(55, sumar(5,5));
    });

});