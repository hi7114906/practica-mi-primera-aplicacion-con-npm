# Práctica| Mi primera aplicación con NPM

En práctica aprendimos a usar un poco de npm, librerias de prueba y tambien logs. 

## Prerequisites

* NPM 
* Node

## Installing

Para ejecutar este proyecto, necesitarás utilizar npm start o ejecutar directamente 
el archivo index.js utilizando node. 
Para ejecutar este proyecto puedes clonar el repositorio en el apartado de clone ó 
también puedes copiar y pegar cada uno de los archivos de javascript en tu editor 
de código favorito.

## Built With

* NodeJS
* Eslint 
* Mocha 
* log4js 

## Contributing

Zaid Joel González Mendoza (353254) Web platforms (7CC2)

## Acknowledgments

Docente: Ing. Luis Antonio Ramírez Martínez
